# thinkphp 6.x 日志扩展
thinkphp 6.x 日志扩展 完善日志信息记录

## 安装和配置
修改项目下的composer.json文件，并添加：  
```
    "cooljun/log4tp":"^1.0.0"
```
然后执行```composer update```。  

安装成功后，添加以下配置到/path/to/phalapi/config/log.php文件：  
```php
    'log4tp' => [
            // 日志记录方式
            'type'           => \Log4tp\Lite::class,
            // 日志保存目录
            'path'           => '',
            // 单文件日志写入
            'single'         => false,
            // 独立日志级别
            'apart_level'    => [],
            // 最大日志文件数量
            'max_files'      => 0,
            // 使用JSON格式记录
            'json'           => false,
            // 日志处理
            'processor'      => null,
            // 关闭通道日志写入
            'close'          => false,
            // 是否实时写入
            'realtime_write' => true,
            // 时间格式化
            'time_format' => "Y-m-d h:i:s.u",
        ],
```

## 使用
使用方式：
```php
    ## 设置default=log4tp使用
    Log::record('测试日志信息，这是警告级别','notice');
    trace('错误信息', 'error');
    Log::info('日志信息{user}', ['user' => '流年']);
    ## 通告channel设置使用
    Log::channel('log4tp')->info('一条测试日志');
```  
## 日志格式
```php
[2023-05-25 03:15:54.979361][sql] | tid:1684998954719_ukQ6 | seq:1.2 | CONNECT:[ UseTime:0.204096s ] mysql:host=rm-2ze85p8459c55q68izo.mysql.rds.aliyuncs.com;port=3306;dbname=vg_zhongcao_test;charset=utf8mb4 | vendor/topthink/think-orm/src/DbManager.php(169)
[2023-05-25 03:15:55.065192][sql] | tid:1684998954719_ukQ6 | seq:1.3 | SHOW FULL COLUMNS FROM `admin` [ RunTime:0.074811s ] | vendor/topthink/think-orm/src/DbManager.php(169)
[2023-05-25 03:15:55.142828][sql] | tid:1684998954719_ukQ6 | seq:1.4 | SELECT * FROM `admin` WHERE  `phone` = '18600765920' LIMIT 1 [ RunTime:0.068835s ] | vendor/topthink/think-orm/src/DbManager.php(169)
[2023-05-25 03:15:55.220319][sql] | tid:1684998954719_ukQ6 | seq:1.5 | UPDATE `admin`  SET `update_time` = 1684998954  WHERE  `id` = 34 [ RunTime:0.063756s ] | vendor/topthink/think-orm/src/DbManager.php(169)
```