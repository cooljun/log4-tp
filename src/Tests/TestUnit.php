<?php
namespace Log4tp\Tests;

use Log4tp\Lite;
use PHPUnit\Framework\TestCase;
use Symfony\Component\VarDumper\VarDumper;
use think\App;

class TestUnit extends TestCase
{
    public function testSave(){
        $server = new Lite(new App(),[
            // 日志保存目录
            'path'           => (new App())->getRuntimePath().'/log/',
            // 单文件日志写入
            'single'         => false,
            // 独立日志级别
            'apart_level'    => [],
            // 最大日志文件数量
            'max_files'      => 0,
            // 使用JSON格式记录
            'json'           => false,
            // 日志处理
            'processor'      => null,
            // 关闭通道日志写入
            'close'          => false,
            // 是否实时写入
            'realtime_write' => true,
            // 时间格式化
            'time_format' => "Y-m-d h:i:s.u"
            ]);
        $server->save(['info'=>['send params data：{"api":"login@index","data":{"phone":18600123122,"password":"111111"}}']]);
    }
}